import { useEffect, useState } from "react";
import { Col, Container, Row, Input } from "reactstrap";
import comarques from './ComarcasList';
import { HorizontalGridLines, LineSeries, VerticalGridLines, XAxis, XYPlot, YAxis } from "react-vis";

const colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
		  '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
		  '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
		  '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
		  '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
		  '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
		  '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
		  '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
		  '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
		  '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];

const Comparativa = () => {

    const [seleccion, setSeleccion] = useState([]);
    const [info, setInfo] = useState([]);
    const [lineas,setLineas] = useState([]);

    useEffect(()=>{
        const apiUrl = `https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?residencia=No&$limit=25000`;
        fetch(apiUrl)
        .then((response) => response.json())
        .then((data) => {
            setInfo(data.sort((a,b)=>{
            return a.data_ini > b.data_ini ? 1 : -1;
            }).filter((el,index) => index%7 === 0));
        })
        .catch((error) => console.log(error));
    }, []);

    const opcionesComarcas = comarques.map((c,index) => {
        return <option key={c.codi} value={c.codi}>{c.nom}</option>;
    });

    const cambia = (e) => {
        let target = e.target;
        let value = Array.from(target.selectedOptions, option => option.value);
        setSeleccion(value);
        let newArrayLineas = [];
        value.forEach((val, index) => {
            let newArray = info.filter((i)=>i.codi === val).map((el,index)=>{
                return {x: new Date(el.data_ini), y: el.ingressos_total};
            });
            newArrayLineas.push(<LineSeries key={val+"-key"} data={newArray} color={colorArray[index]} style={{fill: 'none'}}/>);
        });
        setLineas(newArrayLineas);
    }    

    return(
        <Container fluid>
            <Row>
                <Col>
                    <Input type="select" name="selectMulti" id="selectMulti" multiple onChange={cambia} value={seleccion} className="h-100">
                        {opcionesComarcas}
                    </Input>
                </Col>
                <Col>
                    <XYPlot xType="time" yDomain={[0,100]} width={600} height={300}>
                        <HorizontalGridLines />
                        <VerticalGridLines />
                        <XAxis title="Fecha" />
                        <YAxis title="Casos confirmados" />
                        {lineas}
                    </XYPlot>
                </Col>
            </Row>
        </Container>
    )
}

export default Comparativa;