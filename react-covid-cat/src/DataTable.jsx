import { Table } from "reactstrap";

const DataTable = (props) => {

    const filas = props.dades.map((el,index) => {
        return(
            <tr key={index+"-tr-contact"}>
                <td>{(el.data_ini).split("T")[0].split("-").reverse().join("-")}</td>
                <td>{(el.data_fi).split("T")[0].split("-").reverse().join("-")}</td>
                <td>{el.ingressos_total}</td>
                <td>{el.casos_confirmat}</td>
            </tr>
        );
      });

    return(
        <Table>
            <thead>
                <tr>
                    <th>Data i</th>
                    <th>Data f</th>
                    <th>Ingressos</th>
                    <th>Casos</th>
                </tr>
            </thead>
            <tbody>
                {filas}
            </tbody>
        </Table>
    );
}

export default DataTable;