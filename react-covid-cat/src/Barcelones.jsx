import { Col, Container, Row } from 'reactstrap';
import DataTable from './DataTable';
import DataGrafica from './DataGrafica';

const Barcelones = (props) => {

    return(
        <Container fluid>
            <Row>
                <Col xs={6}>
                <DataTable dades={props.info} />
                </Col>
                <Col xs={6}>
                <DataGrafica dades={props.info} />
                </Col>
            </Row>
        </Container>
    );

}

export default Barcelones;