import { useState, useEffect } from "react";
import { Col, Container, Row, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import DataTable from './DataTable';
import DataGrafica from './DataGrafica';
import comarques from './ComarcasList';

const SelectComarca = () => {

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const toggle = () => setDropdownOpen(prevState => !prevState);

    const [info, setInfo] = useState([]); 
    const [comarca, setComarca] = useState(13);
    

    const toggleComarca = (codi) => setComarca((codi));

    useEffect(() => {
        const apiUrl = ` https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?codi=${comarca}&residencia=No`;
        fetch(apiUrl)
        .then((response) => response.json())
        .then((data) => {
            setInfo(data.sort((a,b)=>{
            return a.data_ini > b.data_ini ? 1 : -1;
            }).filter((el,index) => index%7 === 0));
        })
        .catch((error) => console.log(error));
    }, [comarca]);

    const dropDownComarcas = comarques.map(c => {
        return <DropdownItem key={c.codi} onClick={() => toggleComarca(c.codi)}>{c.nom}</DropdownItem>;
    });

    return(
        <Container fluid>
            <Row>
                <Dropdown direction="right" isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle caret>
                        Escoge Comarca
                    </DropdownToggle>
                    <DropdownMenu>
                        {dropDownComarcas}
                    </DropdownMenu>
                </Dropdown>
            </Row>
            <Row><h3></h3></Row>
            <Row>
                <Col xs={6}>
                    <DataTable dades={info} />
                </Col>
                <Col xs={6}>
                    <DataGrafica dades={info} />
                </Col>
            </Row>
        </Container>
    );
}
export default SelectComarca;