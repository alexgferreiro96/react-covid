import './App.css';
import { useEffect, useState } from 'react';
import Barcelones from './Barcelones';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';
import { Container } from 'reactstrap';
import Home from './Home';
import SelectComarca from './SelectComarca';
import Comparativa from './Comparativa';

const App = () => {

  const [info, setInfo] = useState([]); 

  useEffect(() => {
    const apiUrl = ` https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?codi=13&residencia=No`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setInfo(data.sort((a,b)=>{
          return a.data_ini > b.data_ini ? 1 : -1;
        }).filter((el,index) => index%7 === 0));
      })
      .catch((error) => console.log(error));
  }, []);

  return(
    <BrowserRouter>
      <Container>
        <ul className="nav nav-tabs">
          <li className="nav-item">
            <NavLink exact className="nav-link" to="/">
              Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/barcelones">
            Barcelones
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/triaComarca">
            Tria Comarca
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/comparativa">
            Comparativa
            </NavLink>
          </li>
        </ul>

        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/barcelones" render={()=><Barcelones info={info} />} />
          <Route path="/triaComarca" component={SelectComarca} />
          <Route path="/comparativa" component={Comparativa} />
        </Switch>
      </Container>
    </BrowserRouter>
    
  );
}

export default App;
