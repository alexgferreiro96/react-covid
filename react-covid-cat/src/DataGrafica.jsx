import { HorizontalGridLines, LineSeries, VerticalGridLines, XAxis, XYPlot, YAxis } from "react-vis";

const DataGrafica = (props) => {

    const dataGrafica = props.dades.map((el,index) => {
        return {x: new Date(el.data_ini), y: el.ingressos_total};
    });

    return(
        <XYPlot xType="time" yDomain={[0,500]} width={600} height={300}>
            <HorizontalGridLines />
            <VerticalGridLines />
            <XAxis title="Fecha" />
            <YAxis title="Casos confirmados" />
            <LineSeries data={dataGrafica} color="blue" style={{fill: 'none'}}/>
        </XYPlot>
    );
}

export default DataGrafica;